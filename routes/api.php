<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\Api\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['namespace' => 'Api'], function(){
    //Route::post('/login', [UserController::class, 'createUser']);
    //Route::post('auth/login', [UserController::class, 'loginUser']);

    //define namespace
    Route::post('/login', 'UserController@createUser');
    //authentication layer middleware
    Route::group(['middleware'=>['auth:sanctum']], function(){
        //define some routes middleware
        //get course List
        // Route::any('/courseList', [CourseController::class, 'courseList']);
        Route::any('/courseList', 'CourseController@courseList');
        Route::any('/courseDetail', 'CourseController@courseDetail');
        Route::any('/checkout', 'PayController@checkout');
    });

    //endpoint webHook Stripe
    Route::any('/web_go_hooks', 'PayController@web_go_hooks');
});

