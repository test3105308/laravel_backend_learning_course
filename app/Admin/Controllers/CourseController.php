<?php

namespace App\Admin\Controllers;

use App\Models\User;
use App\Models\CourseType;
use App\Models\CourseTypeController;
use App\Models\Course;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Layout\Content;
use Encore\Admin\Tree;

class CourseController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Course';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Course());

        //the first argument is the database field.
        $grid->column('id', __('Id'));

        //user token change to show username or teacher
        //invoke a function
        $grid->column('user_token', __('Teacher'))->display(function ($token){
            //for further processing data, can create any methods inside it or do operation
            //Laravel eloquent
            return User::where('token', '=', $token)->value('name');
        });
        $grid->column('name', __('Name'));

        //50, 50 refers to the image size
        $grid->column('thumbnail', __('Thumbnail'))->image('', 80, 80);

        $grid->column('description', __('Description'));
        $grid->column('type_id', __('Type id'));
        $grid->column('price', __('Price'));
        $grid->column('lesson_num', __('Lesson num'));
        $grid->column('video_length', __('Video length'));
        $grid->column('downloadable_res', __('Download Number'));
        // $grid->column('follow', __('Follow'));
        // $grid->column('score', __('Score'));
        $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Course::findOrFail($id));

        $show->field('id', __('Id'));
        // $show->field('user_token', __('User token'));
        $show->field('name', __('Name'));
        $show->field('thumbnail', __('Thumbnail'));
        // $show->field('video', __('Video'));
        $show->field('description', __('Description'));
        // $show->field('type_id', __('Type id'));
        $show->field('price', __('Price'));
        $show->field('lesson_num', __('Lesson num'));
        $show->field('video_length', __('Video length'));
        $show->field('downloadable_res', __('Download Number'));
        $show->field('follow', __('Follow'));
        $show->field('score', __('Score'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

      //creating and editing
    protected function form()
    {
        $form = new Form(new Course());
        $form->text('name', __('Name'));
        //Laravel Eloquent returns key value pair
        $result = CourseType::pluck('title', 'id',);

        //retrieve the information
        //select method helps for select one of the options the comes from result variable
        $form->select('type_id', __('Category'))->options($result);

        //show the thumbnails
        $form->image('thumbnail', __('Thumbnail'))->uniqueName();

        //show the video
        $form->file('video', __('Video Course'))->uniqueName();

        //The Description
        $form->text('description', __('Description'));

        //Form for price
        $form->decimal('price', __('Price'));

        //Lesson number
        $form->number('lesson_num', __('Lesson Number'));

        //Length of the video course
        $form->number('video_length', __('Video Length'));

        $form->number('downloadable_res', __('Download Number'));

        //for the posting and who is posting
        $result = User::pluck('name', 'token');
        // dd($result);

        //user token
        $form->select('user_token', __('Teacher'))->options($result);

        //display the Time
        $form->display('created_at', __('Created at'));

        //
        $form->display('updated_at', __('Updated at'));

        //dump and die which means stop executing PHP script so application would be over
        // dd($result);
        // $form->select('type_id', __('Parent Category'))->options((new CourseType())::selectOptions());

        // $form->text('title', __('Course Title'));
        // $form->textarea('description', __('Course Description'));
        // $form->number('order', __('Course Order'));

        return $form;
    }
}
