<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    //
    public function courseList(){

        //API_the_course
        try{
                    //select the fields
        $result = Course::select('name', 'thumbnail', 'lesson_num', 'price', 'id')->get();

        //return response
        return response()->json([
            'code' => 200,
            'msg' => 'My course list is here',
            'data' => $result
        ], 200);
        }catch(\Throwable $throw){
            return response()->json([
                'code' => 500,
                'msg' => 'The column does not exist or you have a syntax error',
                'data' => $throw->getMessage(),
            ], 500);
        }
    }

        //
        public function courseDetail(Request $request){
            //course_id
            $id =  $request->id;

            //API_the_course
            try{
            //select the fields
            $result = Course::where('id', '=', $id)->select(
                'id',
                'name',
                'user_token',
                'description',
                'price',
                'lesson_num',
                'video_length',
                'thumbnail',
                'lesson_num',
                'price',
                'downloadable_res',
                )->first();

            //return response
            return response()->json([
                'code' => 200,
                'msg' => 'My course detail is here',
                'data' => $result
            ], 200);
            }catch(\Throwable $throw){
                return response()->json([
                    'code' => 500,
                    'msg' => 'The column does not exist or you have a syntax error',
                    'data' => $throw->getMessage(),
                ], 500);
            }
        }
}
