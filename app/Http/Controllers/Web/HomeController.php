<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //for security
    public function index(){
        return "Test NGROK";
    }

    //Stripe webHook need this
    public function success(){
        return view('success');
    }

    //Stripe webHook need this
    public function cancel(){
        return "NO";
    }
}
